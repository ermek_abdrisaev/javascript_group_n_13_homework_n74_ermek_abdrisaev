const express = require('express');
const router = express.Router();
const fsReadMessage = require('../fsReadMessage');
const fsMessage = require('../fsMessage');

router.get('/', (req, res) =>{
  let array = fsReadMessage.read();
  return res.send(array);
});

router.post('/', (req, res) =>{
  const message = {
    message: req.body.message,
    datetime: new Date()
  };
  fsMessage.write(message);
  return res.send(message);
});

module.exports = router;