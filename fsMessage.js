const fs = require('fs');
const file = './messages/' + new Date();

module.exports = {
 write(message) {
   fs.writeFile(file, JSON.stringify(message), (err) => {
     if (err) {
       console.log(err);
     } else {
       console.log('File saved');
     }
   });
 }
};