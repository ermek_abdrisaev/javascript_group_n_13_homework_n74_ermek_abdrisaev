const fs = require('fs');

const path = "./messages";
let messages = [];

module.exports = {
  read() {
    fs.readdir(path, (err, files) => {
      let arrM = files.slice(files.length - 5, files.length);
      arrM.forEach(file => {
        if(err){
          console.log(err);
        } else{
          fs.readFile((path + '/' + file), (err, data) => {
            if (err) {
              console.error(err);
            } else  {
              messages.push(JSON.parse(data));
            }
          });
        }
      });
    });
    let array = messages;
    messages = [];
    return array;
  }
};


